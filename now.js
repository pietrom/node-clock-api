function foo() {
  return 19
}

module.exports = function NowResponseBuilder(envName, buildTag) {
  return function() {
    if(!envName) {
      return {}
    }
    return {
        now: new Date().toISOString(),
        environment: envName,
        version: buildTag
    }
  }
}
