const http = require('http')
const { createHttpTerminator } = require('http-terminator')

const port = process.env.PORT || 1919
const envName = process.env.ENVIRONMENT_NAME
const buildTag = process.env.BUILD_TAG

function randomInt(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1))
}

async function sleep(delay) {
    return new Promise((resolve) => {
        setTimeout(resolve, delay)
    })
}

const nowHandler = require('./now')(envName, buildTag)

const handlers = {
    '/now': async (req, res) => {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(nowHandler()))
        res.end()
    },
    '/probes/liveness': async (req, res) => {
        res.writeHead(204)
        res.end()
    },
    '/probes/readiness': async (req, res) => {
        await(randomInt(100, 400))
        res.writeHead(204)
        res.end()
    }
}

const defaultHandler = (req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write(`Hello, K8S World!!! [${envName}: ${buildTag}]`)
    res.end()
}

const server = http.createServer(function (req, res) {
    const handler = handlers[req.url] || defaultHandler
    handler(req, res)
}).listen(port)

const httpTerminator = createHttpTerminator({
  server,
})

function now() {
  return new Date().toISOString()
}

function closed() {
  console.info('Server closed at', now())
}

async function close(signal) {
  console.info(`${signal} signal received at`, now());
  await httpTerminator.terminate()
}

process.on('SIGTERM', () => {
    close('SIGTERM').then(() => console.log('Server stopped at', now()))
})

process.on('SIGINT', () => {
    close('SIGINT').then(() => console.log('Server stopped at', now()))
})

console.info('HTTP server started on port', port, 'at', now())
