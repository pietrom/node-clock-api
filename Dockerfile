FROM node:14.17.1-alpine3.13
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --only=production
COPY . .
EXPOSE 1919
ENTRYPOINT node index.js
