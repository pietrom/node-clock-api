const assert = require('assert')
const {describe, it} = require("mocha")
const NowResponseBuilder = require('../now')
const builder = new NowResponseBuilder('test', 'xxx')
describe('NowResponseBuilder', function() {
    describe('when invoked', function() {
        it('should return provided envName', function() {
            assert.equal(builder().environment, 'test')
        })

        it('should return provided version', function() {
            assert.equal(builder().version, 'xxx')
        })
    })
})
